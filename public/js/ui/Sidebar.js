/**
 * Класс Sidebar отвечает за работу боковой колонки:
 * кнопки скрытия/показа колонки в мобильной версии сайта
 * и за кнопки меню
 * */
class Sidebar {
  /**
   * Запускает initAuthLinks и initToggleButton
   * */
  static init() {
    this.initAuthLinks();
    this.initToggleButton();
  }

  /**
   * Отвечает за скрытие/показа боковой колонки:
   * переключает два класса для body: sidebar-open и sidebar-collapse
   * при нажатии на кнопку .sidebar-toggle
   * */
  static initToggleButton() {
    const toggleButton = document.querySelector('.sidebar-toggle');
    toggleButton.addEventListener('click', () => {
      document.body.classList.toggle('sidebar-open');
      document.body.classList.toggle('sidebar-collapse');
    });
  }

  /**
   * При нажатии на кнопку входа, показывает окно входа
   * (через найденное в App.getModal)
   * При нажатии на кнопку регастрации показывает окно регистрации
   * При нажатии на кнопку выхода вызывает User.logout и по успешному
   * выходу устанавливает App.setState( 'init' )
   * */
  static initAuthLinks() {
    Sidebar.initMenuItemModalAction('.menu-item_login', 'login');
    Sidebar.initMenuItemModalAction('.menu-item_register', 'register');
    Sidebar.initMenuItem('.menu-item_logout', () => {
      User.logout((err, response) => {
        if (err) {
          return;
        }

        if (!response.success) {
          return;
        }

        App.setState('init');
      });
    });
  }

  static initMenuItem(selector, callback) {
    const menuItem = document.querySelector(selector);

    if (!menuItem) {
      return;
    }

    menuItem.addEventListener('click', callback);
  }

  static initMenuItemModalAction(selector, modalName) {
    Sidebar.initMenuItem(selector, () => {
      Sidebar.openModal(modalName);
    });
  }

  static openModal(name = '') {
    const modal = App.getModal(name);
    if (!modal) {
      return;
    }

    modal.open();
  }
}